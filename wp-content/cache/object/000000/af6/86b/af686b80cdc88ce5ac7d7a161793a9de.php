�$1X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:2;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-09-05 11:23:34";s:13:"post_date_gmt";s:19:"2016-09-05 17:23:34";s:12:"post_content";s:15498:"[av_section min_height='100' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='border-extra-arrow-down' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' id='' color='main_color' custom_bg='#2b3e59' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']
[av_one_half first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_image src='http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/logo-heroe.png' attachment='124' attachment_size='full' align='left' styling='' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' animation='no-animation'][/av_image]

[av_textblock size='24' font_color='' color='']
<p class="resumen-hero">A unas cuantas personas y empresas se les han perdonado y dejado de cobrar cantidades multimillonarias de impuestos y adeudos fiscales. ¡Ayúdanos a que las cosas cambien!</p>
<a class="boton-ghost" href="http://privilegiosfiscales.fundar.org.mx/wp-includes/creditos/PrivilegiosFin.pdf">¡Infórmate!</a>
[/av_textblock]

[/av_one_half][av_one_half min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_image src='http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/imagen-heroe.png' attachment='122' attachment_size='full' align='right' styling='no-styling' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' animation='pop-up'][/av_image]

[/av_one_half]
[/av_section]

[av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='scroll' bottom_border_style='scroll' scroll_down='' id='' color='main_color' custom_bg='' src='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' video_mobile_disabled='' overlay_enable='' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']
[av_textblock size='' font_color='' color='']
<h2 class="titulo-1">¿Cuál es el problema?</h2>
[/av_textblock]

[av_one_half first min_height='av-equal-height-column' vertical_alignment='av-align-top' space='' custom_margin='aviaTBcustom_margin' margin='40px' margin_sync='true' padding='15px' padding_sync='true' border='' border_color='' radius='0px,0,0px,0px' background_color='' src='' attachment='' attachment_size='' background_position='top left' background_repeat='no-repeat' animation='pop-up' mobile_display='']

[av_hr class='invisible' height='-5' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_image src='http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/home-adeudos-cancelados-3.png' attachment='144' attachment_size='full' align='center' styling='' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' animation='pop-up'][/av_image]

[av_hr class='invisible' height='-23' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_heading tag='h2' padding='5' heading='Adeudos cancelados' color='' style='blockquote modern-quote modern-centered' custom_font='' size='' subheading_active='' subheading_size='15' custom_class=''][/av_heading]

[av_textblock size='16' font_color='' color='']
<p style="text-align: center;">El gobierno puede dejar de perseguir a los contribuyentes incumplidos y también dejar de cobrarles sus deudas. En tan sólo un año 15 empresas dejaron de pagar deudas por más de 15 mil millones de pesos, un monto equivalente a lo que el gobierno de la república destina a becas en todos los niveles educativos. Haz click para acceder a mayor información relacionada con la cancelación de créditos fiscales.</p>
[/av_textblock]

[av_button label='Leer más' link='page,25' link_target='' size='small' position='center' icon_select='no' icon='ue800' font='entypo-fontello' color='custom' custom_bg='#ffffff' custom_font='#2b3e59']

[/av_one_half][av_one_half min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_image src='http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/condonados-home-2.png' attachment='79' attachment_size='full' align='center' styling='' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' animation='pop-up'][/av_image]

[av_heading tag='h2' padding='5' heading='Adeudos condonados' color='' style='blockquote modern-quote modern-centered' custom_font='' size='' subheading_active='' subheading_size='15' custom_class=''][/av_heading]

[av_textblock size='16' font_color='' color='']
<p style="text-align: center; padding: 0px 10px;">El perdón de adeudos fiscales se realiza sin explicaciones suficientes y a favor de unos cuantos. Su regulación debe cambiar para impedir que los contribuyentes que no necesitan ser beneficiados accedan continuamente a este mecanismo fiscal. Aquí te explicamos más sobre la condonación de créditos fiscales..</p>
[/av_textblock]

[av_button label='Leer más' link='page,23' link_target='' size='small' position='center' icon_select='no' icon='ue800' font='entypo-fontello' color='custom' custom_bg='#ffffff' custom_font='#2b3e59']

[/av_one_half]
[/av_section]

[av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' id='' color='main_color' custom_bg='' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']
[av_textblock size='' font_color='' color='']
<h2 class="titulo-1">¿Qué proponemos?</h2>
[/av_textblock]

[av_one_half first min_height='av-equal-height-column' vertical_alignment='av-align-top' space='no_margin' custom_margin='aviaTBcustom_margin' margin='40px' margin_sync='true' padding='15px' padding_sync='true' border='' border_color='' radius='0px,0,0px,0px' background_color='' src='' attachment='' attachment_size='' background_position='top left' background_repeat='no-repeat' animation='pop-up' mobile_display='']

[av_image src='http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/dummies-home-v5-implementacion.png' attachment='118' attachment_size='full' align='left' styling='' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' animation='av-rotateInUpRight'][/av_image]

[/av_one_half][av_one_half min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_iconlist position='left' iconlist_styling='av-iconlist-small' custom_title_size='22' custom_content_size='14' font_color='' custom_title='' custom_content='' color='custom' custom_bg='#ffffff' custom_font='#ffffff' custom_border='#ffffff']
[av_iconlist_item title='1' link='' linktarget='' linkelement='' icon='ue879' font='entypo-fontello']
<strong>Mayor transparencia.</strong> Gracias a la nueva Ley General de Transparencia y Acceso a la Información (LGTAIP) hemos logrado que a partir de julio de 2015 el SAT publique la información de los contribuyentes que han recibido cancelaciones y condonaciones de créditos fiscales. Pero esto no es suficiente. Necesitamos conocer quiénes se beneficiaron de estos mecanismos en el pasado. El INAI ha ordenado entregar esta información en 32 ocasiones y el SAT ha incumplido. Recordemos que sus resoluciones son definitivas e inatacables. Conocer esta información es fundamental para poder discutir publicamente sobre estas políticas y sus resultados y juntos, sociedad y gobierno, encontrar soluciones.
[/av_iconlist_item]
[av_iconlist_item title='2' link='' linktarget='' linkelement='' icon='ue879' font='entypo-fontello']
<strong>Rendición de cuentas.</strong> No quedan claras las acciones que lleva a cabo el gobierno para cobrar deudas fiscales, sobre todo a los contribuyentes más grandes, y tampoco los criterios para perdonar los adeudos. Necesitamos explicaciones detalladas para asegurarnos de que todos paguemos lo que es justo.
[/av_iconlist_item]
[av_iconlist_item title='3' link='' linktarget='' linkelement='' icon='ue879' font='entypo-fontello']
<strong>Impulsar una genda legislativa para terminar con los #PrivilegiosFiscales.</strong> Los últimos años hemos vivido bajo la lógica del "recorte al presupuesto". Es hora de cerrar filas y aseguranos de que todos y todas paguemos lo justo de impuestos.
[/av_iconlist_item]
[/av_iconlist]

[av_button label='Leer más' link='page,27' link_target='' size='small' position='left' icon_select='no' icon='ue800' font='entypo-fontello' color='custom' custom_bg='#ffffff' custom_font='#2b3e59']

[/av_one_half]
[/av_section]

[av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='scroll' bottom_border_style='scroll' scroll_down='' id='' color='main_color' custom_bg='' src='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' video_mobile_disabled='' overlay_enable='' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']
[av_textblock size='' font_color='' color='']
<h2 class="titulo-1">¿Cómo llegamos aquí?</h2>
[/av_textblock]

[av_one_half first min_height='av-equal-height-column' vertical_alignment='av-align-top' space='no_margin' custom_margin='aviaTBcustom_margin' margin='40px' margin_sync='true' padding='15px' padding_sync='true' border='' border_color='' radius='0px,0,0px,0px' background_color='' src='' attachment='' attachment_size='' background_position='top left' background_repeat='no-repeat' animation='pop-up' mobile_display='']

[av_image src='http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/home-timeline-5.png' attachment='149' attachment_size='full' align='left' styling='' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' animation='pop-up'][/av_image]

[/av_one_half][av_one_half min_height='' vertical_alignment='av-align-top' space='' margin='0px' margin_sync='true' padding='0px' padding_sync='true' border='' border_color='' radius='0px' radius_sync='true' background_color='' src='' attachment='' attachment_size='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_hr class='invisible' height='-10' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_iconlist position='left' iconlist_styling='av-iconlist-small' custom_title_size='22' custom_content_size='16' font_color='' custom_title='' custom_content='' color='custom' custom_bg='#ffffff' custom_font='#282f56' custom_border='#ffffff']
[av_iconlist_item title='2010' link='' linktarget='' linkelement='' icon='ue879' font='entypo-fontello']
Fundar reclamó la inconstitucionalidad del Art. 69 del Código Fiscal de la Federación, principal candado para conocer quiénes reciben <strong>#PrivilegiosFiscales</strong>
[/av_iconlist_item]
[av_iconlist_item title='2013' link='' linktarget='' linkelement='' icon='ue879' font='entypo-fontello']
Reforma al CFF (Art. 69) que hizo posible conocer a los contribuyentes que reciben una condonación o cancelación de créditos fiscales. Esta reforma no permitió conocer los montos de créditos cancelados o condonados.
[/av_iconlist_item]
[av_iconlist_item title='2015' link='' linktarget='' linkelement='' icon='ue879' font='entypo-fontello']
Publicación de la Ley General de Transparencia y Acceso a la Información Pública (LGTAIP). Gracias a los esfuerzos de la sociedad civil la LGTAIP incluyó la obligación de publicar el nombre, monto y RFC de los contribuyentes que reciben <strong>#PrivilegiosFiscales.</strong>
[/av_iconlist_item]
[av_iconlist_item title='2016' link='' linktarget='' linkelement='' icon='ue879' font='entypo-fontello']
Aún continúa negándose el acceso a mucha información y no se han explicado muchas cosas.. <strong>¡No más #PrivilegiosFiscales!</strong>
[/av_iconlist_item]
[/av_iconlist]

[av_button label='Leer más' link='page,29' link_target='' size='small' position='left' icon_select='no' icon='ue800' font='entypo-fontello' color='custom' custom_bg='#ffffff' custom_font='#2b3e59']

[/av_one_half]
[/av_section]

[av_section min_height='' min_height_px='500px' padding='default' shadow='no-border-styling' bottom_border='border-extra-arrow-down' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' id='logos-ongs' color='main_color' custom_bg='#f6f3f2' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']

[av_one_full first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_textblock size='' font_color='' color='']
<h2 class="titulo-1">Aliados</h2>
[logooos columns="3" layout="slider" category="-1" orderby="date" order="DESC" tooltip="disabled" responsive="enabled" grayscale="enabled" border="enabled" bordercolor="#f6f3f2" borderradius="logooos_no_radius" autoplay="false" scrollduration="1000" pauseduration="9000" buttonsbordercolor="#f6f3f2" buttonsbgcolor="#f6f3f2" buttonsarrowscolor="lightgray" hovereffect="effect4" ]
[/av_textblock]

[/av_one_full][/av_section]";s:10:"post_title";s:6:"Inicio";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:6:"inicio";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-09-19 15:40:52";s:17:"post_modified_gmt";s:19:"2016-09-19 21:40:52";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:51:"http://privilegiosfiscales.fundar.org.mx/?page_id=2";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}