1X<?php exit; ?>a:1:{s:7:"content";a:14:{s:10:"_edit_last";a:1:{i:0;s:1:"1";}s:10:"_edit_lock";a:1:{i:0;s:12:"1474310948:1";}s:17:"_wp_page_template";a:1:{i:0;s:30:"template-AdeudosCondonados.php";}s:31:"fw:opt:ext:pb:page-builder:json";a:1:{i:0;s:2:"[]";}s:10:"fw_options";a:1:{i:0;s:158:"a:3:{s:12:"page-builder";a:2:{s:4:"json";s:2:"[]";s:14:"builder_active";b:0;}s:22:"seo-titles-metas-title";s:0:"";s:28:"seo-titles-metas-description";s:0:"";}";}s:25:"_aviaLayoutBuilder_active";a:1:{i:0;s:6:"active";}s:27:"_aviaLayoutBuilderCleanData";a:1:{i:0;s:7733:"[av_section min_height='' min_height_px='500px' padding='default' shadow='no-border-styling' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' id='' color='main_color' custom_bg='' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']

[av_one_full first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_textblock size='' font_color='' color='']
<img class="alignnone size-full wp-image-7775 avia-image-container avia_animated_image avia_animate_when_almost_visible av-rotateIn av-styling- avia-builder-el-no-sibling avia-align-center avia_start_animation avia_start_delayed_animation" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/condonaciones.png" width="524" height="353" />
[/av_textblock]

[av_hr class='invisible' height='-60' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_heading tag='h2' padding='10' heading='ADEUDOS PERDONADOS' color='' style='blockquote modern-quote modern-centered' custom_font='' size='54' subheading_active='subheading_below' subheading_size='25' custom_class='']
(Condonación de créditos fiscales)
[/av_heading]

[av_hr class='invisible' height='-30' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_textblock size='20' font_color='' color='']
<p style="text-align: center;">En teoría, el gobierno puede perdonar impuestos, multas y otro tipo de deudas fiscales con la finalidad de apoyar económicamente a personas o sectores económicos con baja capacidad de contributiva, o bien, como una manera de incentivar a los contribuyentes para ponerse al corriente de sus obligaciones y así lograr captar más recursos en el corto plazo.</p>
<p style="text-align: center;"></p>
[/av_textblock]

[/av_one_full][av_hr class='invisible' height='-10' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_heading tag='h2' padding='40' heading='¿Cuál es el problema?' color='' style='blockquote modern-quote modern-centered' custom_font='' size='34' subheading_active='' subheading_size='15' custom_class=''][/av_heading]

[av_hr class='invisible' height='20' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_textblock size='' font_color='' color='']
<ul class="avia-icon-list avia-icon-list-left av-iconlist-big avia_animate_when_almost_visible avia_start_animation">
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">1</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Formulación discrecional</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

Cuando el gobierno decide crear nuevas posibilidades para que las personas reciban el perdón de sus adeudos, pocas veces se profundiza en torno a la necesidad de beneficiar a las personas o a los sectores económicos a quienes van dirigidas estas medidas. Tampoco se analiza a profundidad si la condonación es la mejor forma para lograr que el gobierno incremente su eficiencia recaudatoria.</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">2</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Diseño sin controles</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

Las distintas modalidades de condonación de créditos fiscales no se acompañan de medidas normativas para garantizar que los contribuyentes no sean beneficiados en repetidas ocasiones y de manera injustificada. El perdón de créditos debe percibirse como un evento único, extraordinario y de difícil acceso, ya que su repetición periódica y a favor de los sectores económicos con capacidad contributiva puede incentivar a que los contribuyentes prefieran eludir sus obligaciones bajo la expectativa de que algún día sus adeudos serán perdonados.</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">3</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Poca transparencia y falta de rendición de cuentas</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

Pese a que la LGTAIP no distingue entre los tipos de condonaciones que deben publicarse, el SAT no pública la información sobre todos los tipos de condonaciones que existen. La autoridad tributaria tampoco explica a detalle cuáles fueron los fundamentos y los motivos que tomó en consideración para perdonar créditos fiscales, y continúa haciendo uso del secreto fiscal para impedir el acceso de otra información relevante.</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">4</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Beneficios para unos cuantos</div>
</header>
<p class="iconlist_content " style="font-size: 16px;">La información disponible públicamente revela que la condonación ha beneficiado principalmente a quienes tienen mayores adeudos, lo cual arroja serias dudas en torno a la necesidad de beneficiar a los contribuyentes que, en principio, tienen mayor capacidad económica y cuyos recursos podrían haber sido captados sin la necesidad de otorgarles la condonación de adeudos.</p>

En el último año, por ejemplo, más de 1 383 personas morales recibieron la condonación de créditos  por un monto total de 4 400.69 millones, sin embargo sólo el 1% de estas empresas acumularon el 85% del total de créditos condonados en este periodo. 

Algo similar sucedió en 2013, año en que se implementó un programa masivo de amnistía fiscal en donde a 36 empresas (de un universo total de 41 399) les perdonaron más de 80 000 millones de pesos, casi la mitad de lo que se condonó gracias a este programa.</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
</ul>
[/av_textblock]

[/av_section]";}s:6:"layout";a:1:{i:0;s:8:"fullsize";}s:7:"sidebar";a:1:{i:0;s:0:"";}s:6:"footer";a:1:{i:0;s:0:"";}s:16:"header_title_bar";a:1:{i:0;s:0:"";}s:19:"header_transparency";a:1:{i:0;s:0:"";}s:25:"_avia_hide_featured_image";a:1:{i:0;s:1:"0";}s:28:"_avia_builder_shortcode_tree";a:1:{i:0;s:804:"a:1:{i:0;a:3:{s:3:"tag";s:10:"av_section";s:7:"content";a:5:{i:0;a:3:{s:3:"tag";s:11:"av_one_full";s:7:"content";a:5:{i:0;a:3:{s:3:"tag";s:12:"av_textblock";s:7:"content";a:0:{}s:5:"index";i:2;}i:1;a:3:{s:3:"tag";s:5:"av_hr";s:7:"content";a:0:{}s:5:"index";i:3;}i:2;a:3:{s:3:"tag";s:10:"av_heading";s:7:"content";a:0:{}s:5:"index";i:4;}i:3;a:3:{s:3:"tag";s:5:"av_hr";s:7:"content";a:0:{}s:5:"index";i:5;}i:4;a:3:{s:3:"tag";s:12:"av_textblock";s:7:"content";a:0:{}s:5:"index";i:6;}}s:5:"index";i:1;}i:1;a:3:{s:3:"tag";s:5:"av_hr";s:7:"content";a:0:{}s:5:"index";i:7;}i:2;a:3:{s:3:"tag";s:10:"av_heading";s:7:"content";a:0:{}s:5:"index";i:8;}i:3;a:3:{s:3:"tag";s:5:"av_hr";s:7:"content";a:0:{}s:5:"index";i:9;}i:4;a:3:{s:3:"tag";s:12:"av_textblock";s:7:"content";a:0:{}s:5:"index";i:10;}}s:5:"index";i:0;}}";}}}