�$1X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:25;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-09-12 20:49:44";s:13:"post_date_gmt";s:19:"2016-09-13 02:49:44";s:12:"post_content";s:5473:"[av_section min_height='' min_height_px='500px' padding='default' shadow='no-border-styling' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' id='' color='main_color' custom_bg='' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']

[av_one_full first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_textblock size='' font_color='' color='']
<img class="alignnone size-full wp-image-5136 avia-image-container avia_animated_image avia_animate_when_almost_visible av-rotateIn av-styling- avia-builder-el-no-sibling avia-align-center avia_start_animation avia_start_delayed_animation" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/Asset-cancelados-2.png" width="506" height="259" /> [/av_textblock]

[av_hr class='invisible' height='-60' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_heading tag='h2' padding='10' heading='ADEUDOS NO COBRADOS' color='' style='blockquote modern-quote modern-centered' custom_font='' size='54' subheading_active='subheading_below' subheading_size='25' custom_class='']
(Cancelación de créditos fiscales)
[/av_heading]

[av_hr class='invisible' height='-30' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_textblock size='18' font_color='' color='']
<p style="text-align: center;">El SAT puede cancelar adeudos fiscales cuando le resulte muy costoso cobrarlos o cuando los deudores o responsables solidarios no tengan bienes para pagar sus deudas o no puedan ser localizados. En teoría, esto se realiza para mejorar su eficiencia recaudatoria y dirigir sus esfuerzos hacia la recuperación de otros adeudos.</p>
[/av_textblock]

[/av_one_full][av_hr class='invisible' height='-10' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_heading tag='h2' padding='40' heading='¿Cuál es el problema?' color='' style='blockquote modern-quote modern-centered' custom_font='' size='34' subheading_active='' subheading_size='15' custom_class=''][/av_heading]

[av_hr class='invisible' height='20' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_textblock size='' font_color='' color='']
<ul class="avia-icon-list avia-icon-list-left av-iconlist-big avia_animate_when_almost_visible avia_start_animation">
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">1</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Falta de rendición de cuentas.</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

Puesto que el gobierno se ha negado sistemáticamente a rendir cuentas sobre su gestión, es imposible saber en qué casos el SAT decide cancelar créditos de manera justificada. En otras palabras, desconocemos si la autoridad tributaria intenta recuperar los adeudos hasta el máximo de sus posibilidades antes de decidir cancelarlos. Esto es todavía más alarmante porque las personas que deben más, son a quienes principalmente se les dejan de cobrar sus deudas.

</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">2</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Beneficios para unos cuantos.</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

En el periodo julio 2015 - julio 2016, el SAT canceló 58 150 millones de pesos. Esta cifra equivale a lo que ejerció toda la SEMARNAT durante 2015 (57 452 millones). En total, 9 110 personas personas físicas fueron beneficiadas con un monto total acumulado de 6 511.83 millones de pesos; y 5 865 personas morales por un monto total de 51 638.32 millones de pesos.

Las personas físicas y morales que poseen más adeudos son a quienes el gobierno ha dejado de cobrarles impuestos y otras deudas tributarias. En las siguientes gráficas es posible comprobar esta situación

</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
</ul>
[/av_textblock]

[/av_section]";s:10:"post_title";s:18:"Adeudos cancelados";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:18:"adeudos-cancelados";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-09-19 12:42:45";s:17:"post_modified_gmt";s:19:"2016-09-19 18:42:45";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:52:"http://privilegiosfiscales.fundar.org.mx/?page_id=25";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}