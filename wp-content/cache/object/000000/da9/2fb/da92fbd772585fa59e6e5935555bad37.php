�$1X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:27;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-09-12 20:50:54";s:13:"post_date_gmt";s:19:"2016-09-13 02:50:54";s:12:"post_content";s:7886:"[av_section min_height='' min_height_px='500px' padding='default' shadow='no-border-styling' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' id='' color='main_color' custom_bg='' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='']

[av_textblock size='' font_color='' color='']
<img class="alignnone size-full wp-image-94230 avia-image-container avia_animated_image avia_animate_when_almost_visible av-rotateIn av-styling- avia-builder-el-no-sibling avia-align-center avia_start_animation avia_start_delayed_animation" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/propuestas-2.png" width="398" height="229" />
[/av_textblock]

[av_one_full first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' padding='0px' border='' border_color='' radius='0px' background_color='' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_display='']

[av_heading heading='¿QUÉ PROPONEMOS?' tag='h2' style='blockquote modern-quote modern-centered' size='54' subheading_active='' subheading_size='25' padding='10' color='' custom_font='']
(cancelación de créditos fiscales)
[/av_heading]

[/av_one_full][av_hr class='invisible' height='20' shadow='no-shadow' position='center' custom_border='av-border-thin' custom_width='50px' custom_border_color='' custom_margin_top='30px' custom_margin_bottom='30px' icon_select='yes' custom_icon_color='' icon='ue808' font='entypo-fontello']

[av_textblock size='' font_color='' color='']
<ul class="avia-icon-list avia-icon-list-left av-iconlist-big avia_animate_when_almost_visible avia_start_animation">
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">1</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Una política de cancelaciones y condonaciones de créditos fiscales más transparente</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

A pesar de los avances en materia de transparencia, el secreto fiscal y la poca disposición por parte de la autoridad tributaria para cumplir a cabalidad con sus obligaciones en materia de transparencia continúan limitando el acceso a la información de la ciudadanía. El secreto fiscal debe ser superado para que sea posible conocer quienes han sido beneficiados con la cancelación o condonación de sus créditos antes del 2015. También es importante que el SAT cumpla con las múltiples resoluciones del INAI en donde se le ha ordenado entregar esta información.

Las normas que regulan la cancelación y condonación de créditos deberían incluir obligaciones de transparencia adicionales a las que ya existen. También es recomendable que la autoridad tributaria publique la información en su poder sobre otro tipo de figuras que equivalen a una condonación de créditos, como el caso de reducciones o disminuciones de los mismos. Esta información es de interés público y debería de ser un factor para brindar certeza a todos y todas las contribuyentes de que la autoridad fiscal no realiza tratos desiguales.

</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">2</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Mayor rendición de cuentas en relación a los esfuerzos por recuperar los créditos fiscales</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

Para generar credibilidad es fundamental saber que se están realizando todos los esfuerzos para cobrar deudas tributarias a aquellos contribuyentes que sí cuentan con capacidad de pago, y de esta forma evitar que se abuse de las figuras de cancelaciones y condonaciones de créditos.

Para lograr lo anterior, es necesario que la autoridad tributaria en México dé a conocer información respecto a los fundamentos y motivos que justificaron la cancelación o condonación de cada crédito fiscal, además de las acciones que fueron intentadas para recuperar los adeudos fiscales antes de beneficiar a los contribuyentes. Al respecto, es fundamental que la autoridad tributaria rinda cuentas sobre lo siguiente:

Para los créditos cancelados:
<ul>
 	<li>Si los créditos fueron cancelados por incosteabilidad, debe darse a conocer la antigüedad del crédito y la razón por la cual la autoridad consideró que su probabilidad de cobro era mínima. Además, una explicación exhaustiva permitiría conocer los actos de fiscalización implementados por la autoridad para intentar recuperar el crédito previo a su cancelación.</li>
 	<li>Si los créditos fueron cancelados por insolvencia del deudor, deben explicarse las acciones que fueron implementadas por la autoridad para evaluar si la persona tenía bienes susceptibles de embargo, para intentar identificar a los contribuyentes o para determinar si las personas fallecidas dejaron bienes que pudiesen cubrir el crédito.</li>
</ul>
Para los créditos condonados:
<ul>
 	<li>Debe explicarse el supuesto normativo bajo el cual se otorgó la condonación de un crédito fiscal.</li>
 	<li>Motivos de la autoridad para proceder con la condonación de cada uno de los créditos fiscales. La autoridad debe demostrar que las personas beneficiadas se ajustaron a los supuestos normativos en materia de condonación de créditos.</li>
 	<li>La autoridad también debería explicar cuáles fueron los actos de fiscalización que se implementaron antes de que la condonación de créditos fiscales tuviera lugar.Finalmente, también es fundamental que para ambos supuestos, al momento de rendir cuentas sobre su gestión, la autoridad tributaria individualice los tipos y los montos de cada crédito fiscal, además de la fecha exacta en que las personas recibieron el beneficio y el tiempo que transcurrió entre la generación de la deuda y el momento de su cancelación o perdón.</li>
</ul>
</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
 	<li class="avia_start_animation">
<div class="iconlist_icon avia-font-entypo-fontello preguntas">3</div>
<article class="article-icon-entry ">
<div class="iconlist_content_wrap"><header class="entry-content-header">
<div class="iconlist_title_big preg-titulo">Una agenda legislativa que permita atacar de raíz los privilegios fiscales injustificados</div>
</header>
<div class="iconlist_content " style="font-size: 16px;">

En el marco de los problemas que presentan las finanzas públicas en México, resulta prioritario plantear modificaciones normativas que disminuyan la probabilidad de que ciertos contribuyentes no paguen lo que es justo. Antes de otorgar beneficios fiscales, el Estado debería acreditar plenamente que estas medidas son necesarias para alcanzar los objetivos económicos deseados y la eficiencia recaudatoria.

Por otro lado, también es importante promover una discusión y revisión del marco normativo y las figuras jurídicas que podrían ser la raíz de la poca capacidad que tiene la autoridad tributaria para recuperar créditos fiscales, como el caso de la responsabilidad solidaria y la responsabilidad de sociedades anónimas.

</div>
</div>
<footer class="entry-footer"></footer></article>
<div class="iconlist-timeline"></div></li>
</ul>
[/av_textblock]

[/av_section]";s:10:"post_title";s:18:"¿Qué proponemos?";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"que-proponemos";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-09-20 07:12:21";s:17:"post_modified_gmt";s:19:"2016-09-20 13:12:21";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:52:"http://privilegiosfiscales.fundar.org.mx/?page_id=27";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}