<?php 
	/*
	Template Name: Conversion
	*/
	
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 	 get_header();


 	 if( get_post_meta(get_the_ID(), 'header', true) != 'no') echo avia_title();
 	 
 	 do_action( 'ava_after_main_title' );
	 
	/**
	 $tuitkey=rand(0,8);
	 $tuits=array("",
				  "Como contribuyente quiero un trato igual del @SATMX y por eso exigo se publique la informacion de quiénes reciben #PrivilegiosFiscales",
				  "Merecemos una explicación detallada del @SATMX sobre los #PrivilegiosFiscales Necesitamos saber qué medidas se toman para evitar la evasión",
				  "No quedan claras las estrategias del @SATMX para cobrar deudas fiscales a grandes contribuyentes. Ya basta de #PrivilegiosFiscales",
				  "Impuestos se perdonan discrecionalmente y @SATMX se niega a publicar el nombre de estas empresas, a pesar de que la ley lo obliga a hacerlo",
				  "¿A quiénes protege el @SATMX? Exige con nosotros que terminen los #PrivilegiosFiscales",
				  "En el último año, el @SATMX decidió dejar de cobrar impuestos y otro tipo de deudas a 570 empresas por un total de 40 mil millones de pesos",
				  ".@SATMX deja de perseguir y cobrar deudas a contribuyentes incumplidos. En un año 15 empresas dejaron de pagar 15 mil millones de pesos.",
				  "Lo que @SATMX deja de cobrar a grandes empresas equivale a lo que el @gobmx destina a becas en todos los niveles educativos",
				  "El perdón de adeudos fiscales a unas cuantas personas y empresas se realiza sin explicaciones suficientes ¡No más #PrivilegiosFiscales!",				  
				  );
	 $pretuit=$tuits[$tuitkey];
	 $tuit=urlencode($pretuit);
	 **/
	 
	 ?> 
	 
	 
	 
	 <!-- zona hero conversion -->
	 <div class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll  avia-builder-el-0  el_before_av_section  avia-builder-el-first  av-minimum-height av-minimum-height-100 container_wrap fullsize" id="av_section_hero-zone-conversion">
		<div class="container">
			<main class="template-page content av-content-full alpha units" role="main" style="padding: 0px;">		
					<div class="entry-content-wrapper clearfix">
						<div style="border-radius:0px; " class="flex_column av_one_half  flex_column_div av-zero-column-padding first  avia-builder-el-1  el_before_av_one_half  avia-builder-el-first  ">
							<div itemtype="https://schema.org/ImageObject" itemscope="itemscope" class="avia-image-container  av-styling-   avia-builder-el-2  el_before_av_textblock  avia-builder-el-first  avia-align-left ">
								<div class="avia-image-container-inner">
									<img itemprop="contentURL" title="logo-heroe" alt="" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/logo-heroe.png" class="avia_image privilegios-conver">
								</div>
							</div>
							<section itemtype="https://schema.org/CreativeWork" itemscope="itemscope" class="av_textblock_section">
								<div itemprop="text" style="font-size:24px; " class="avia_textblock ">
									<a class="ref-participa" name="participa"><p class="resumen-hero-converversion">A unas cuantas personas se les han perdonado y dejado de cobrar cantidades multimillonarias de impuestos y adeudos fiscales ¡Ayúdanos a que las cosas cambien!</p></a>
									
										<!-- Begin MailChimp Signup Form -->
										<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
										<div id="mc_embed_signup">
										<form id="mc-embedded-subscribe-form" class="validate" action="//fundar.us7.list-manage.com/subscribe/post?u=300e7ac6c945b1cff1e9f93e5&amp;id=3cfa62574c" method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="" style="padding-left: 0px;">
											<div id="mc_embed_signup_scroll">
											<label for="mce-EMAIL">Para mantenerte informado, ¡suscríbete!</label>
											<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Correo electrónico" required>
											<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
											<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8ffeaadb8bee47fad25dbd84e_206f70b756" tabindex="-1" value=""></div>
											<div  class="cleard"><img class="clue" src="/wp-content/themes/privilegios/images/cue-arrow.png" />
										<input type="submit" value="Quiero participar" name="Subscribe" id="btn-conversion-mail" class="avia-button-hero"></div>
											</div>
										</form>
										</div>

										<!--End mc_embed_signup-->
									
								</div>
							</section>
						</div>
						<div style="border-radius:0px; " class="flex_column av_one_half  flex_column_div av-zero-column-padding   avia-builder-el-4  el_after_av_one_half  avia-builder-el-last  ">
							<div itemtype="https://schema.org/ImageObject" itemscope="itemscope" class="avia-image-container avia_animated_image avia_animate_when_almost_visible pop-up av-styling-no-styling   avia-builder-el-5  avia-builder-el-no-sibling  avia-align-right  avia_start_animation avia_start_delayed_animation">
								<div class="avia-image-container-inner">
									<img alt="" src="/wp-content/themes/privilegios/images/hero-img-conversion.png" class="avia_image ilustracion-hero">
								</div>
							</div>
						</div>
					</div>
			</main><!-- close content main element -->
		</div>
	 </div>  <!-- /zona hero conversion -->
	 
	 <div id="av_section_conversiones" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-20 el_after_av_section el_before_av_section container_wrap fullsize">
		
		<main style="margin-top: 0px; padding-top: 0px;" class='template-page content  <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>

                                <?php if (have_posts()) :
                                	while (have_posts()) : the_post(); ?>

		                                <article class='post-entry post-entry-type-page entry'>

			                                <div class="entry-content-wrapper clearfix">
                                                <?php
                                                echo '<div class="entry-content" '.avia_markup_helper(array('context' => 'entry_content','echo'=>false)).'>';
                                                    the_content(__('Read more','avia_framework').'<span class="more-link-arrow">  &rarr;</span>');
                                                echo '</div>';

                                                echo '<footer class="entry-footer">';
                                                wp_link_pages(array('before' =>'<div class="pagination_split_post">',
                                                                        'after'  =>'</div>',
                                                                        'pagelink' => '<span>%</span>'
                                        ));
                                                echo '</footer>';
                
                                                do_action('ava_after_content', get_the_ID(), 'page');
                                                ?>
		                                	</div>

		                                </article><!--end post-entry-->


                                <?php
	                                $post_loop_count++;
	                                endwhile;
	                                else:
                                ?>

                                    <article class="entry">
                                        <header class="entry-content-header">
                                            <h1 class='post-title entry-title'><?php _e('Nothing Found', 'avia_framework'); ?></h1>
                                        </header>

                                        <?php get_template_part('includes/error404'); ?>

                                        <footer class="entry-footer"></footer>
                                    </article>

                                <?php

	                                endif;
                                ?>

		</main>

		<?php

		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();

		?>
	 </div>

			

	

<!-- Google Code for suscripcion a privilegios fiscales Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963761815;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "BWAzCNbx1moQl63HywM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/963761815/?label=BWAzCNbx1moQl63HywM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<?php get_footer(); ?>
