<?php 
	/*
	Template Name: Adeudos Cancelados
	*/
	
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 	 get_header();

	 ?>

	<style>

		.bar {
		  fill: steelblue;
		}

		.bar:hover {
		  fill: brown;
		  cursor:pointer; cursor: hand;
		  opacity: 0.7;

		}

		div.tooltip { 
		    position: relative;    
		    left:175px; 
		    top:160px; 
		    text-align: center;     
		    line-height: 1;
		    padding: 12px;
		    background: rgba(100, 100, 100, 0.6);
		    color: #fff;
		    border-radius: 5px;
		    min-width: 450px;
		    max-width: 450px;
		    min-height: 150px;
		    max-height: 150px;
		}

		.axis {
		  font: 10px sans-serif;
		}



		.axis path,
		.axis line {
		  fill: none;
		  stroke: #000;
		  shape-rendering: crispEdges;
		}

		#graph_area{
		  max-width: 760px;
		  min-width: 760px;
		  margin-top: -240px;
		}

		#barChart{
		  margin-top: -125px;
		}


		.replay{
		  color: #2b3e59;
		  padding: 15px 32px;
		  text-align: center;
		  text-decoration: none;
		  font-size: 16px;
		  cursor: pointer;
		  margin-top: 125px;
		  margin-right: 10px;
		  background: transparent;
		  border: 2px solid #2b3e59;
		}

		#r1{
		  background: #2b3e59;
		  color:#fff;
		}

		#r1:hover{
		  background: #2b3e59;
		  color:#fff;
		}

		#r2:hover{
		  background: #2b3e59;
		  color:#fff;
		}

		#message{
		  text-align: center;
		}

		@media (max-width: 760px) {
		  .replay{
		    padding: 5px 10px;
		    text-align: center;
		    text-decoration: none;
		    font-size: 10px;
		    cursor: pointer;
		    float:right;
		    margin-top: 125px;
		    margin-right: 10px;
		    background: transparent;
		  }

		  #graph_area{
		    max-width: 100%;
		    min-width: 100% ;
		  }

		  div.tooltip { 
		    line-height: 1;
		    padding: 3px;
		    position: block;
		    min-width: 80%;
		    max-width: 80%;
		    min-height: 70%;
		    max-height: 70%;
		    float:center;
		    left:25%;
		  }
		}

	</style>
	 
	 
	 
        <!-- cabecera-->
         <div class="avia-section-cabecera" id="av_section_hero-zone-conversion">
		<div class="container" style="padding: 0px;">
			<a class="cabecera-in" href="http://privilegiosfiscales.fundar.org.mx/"><img class="avia_image cancelados-logo" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/logo-cabecera2.png" itemprop="contentURL" /></a>
		</div>
	 </div><!-- /cabecera -->
	 
	 <div id="av_section_conversiones" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-20 el_after_av_section el_before_av_section container_wrap fullsize">
		
		<main style="margin-top: 0px; padding-top: 0px;" class='template-page content  <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>

                                <?php if (have_posts()) :
                                	while (have_posts()) : the_post(); ?>

		                                <article class='post-entry post-entry-type-page entry'>

			                                <div class="entry-content-wrapper clearfix">
                                                <?php
                                                echo '<div class="entry-content" '.avia_markup_helper(array('context' => 'entry_content','echo'=>false)).'>';
                                                    the_content(__('Read more','avia_framework').'<span class="more-link-arrow">  &rarr;</span>');
                                                echo '</div>';

                                                echo '<footer class="entry-footer">';
                                                wp_link_pages(array('before' =>'<div class="pagination_split_post">',
                                                                        'after'  =>'</div>',
                                                                        'pagelink' => '<span>%</span>'
                                        ));
                                                echo '</footer>';
                
                                                do_action('ava_after_content', get_the_ID(), 'page');
                                                ?>
		                                	</div>

		                                </article><!--end post-entry-->


                                <?php
	                                $post_loop_count++;
	                                endwhile;
	                                else:
                                ?>

                                    <article class="entry">
                                        <header class="entry-content-header">
                                            <h1 class='post-title entry-title'><?php _e('Nothing Found', 'avia_framework'); ?></h1>
                                        </header>

                                        <?php get_template_part('includes/error404'); ?>

                                        <footer class="entry-footer"></footer>
                                    </article>

                                <?php

	                                endif;
                                ?>
				
	<!-- grafica-->
         <div id="av_section_1" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-0 avia-builder-el-no-sibling container_wrap fullsize" style="background-color: #edae44; background-color: #fff;">
		<div class="container"">
			<main id="graph_area" class="template-page content av-content-full alpha units" itemprop="mainContentOfPage" role="main">
			    <button class="replay" id="r2"> Ver Personas Físicas </button>
			    <button class="replay" id="r1"> Ver Personas Morales </button>
			    <div id="barChart"></div>
			    <div id="message"></div>
			</main>


		</div>
		
	 </div><!-- /grafica-->
	 
	 <!-- botón descarga investigación-->
	 <div id="av_section_btnInvs" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-0 avia-builder-el-no-sibling container_wrap fullsize" style="background-color: #edae44; background-color: #fff;">
	 <div class="container"">
	 		<div class="post-entry post-entry-type-page">
		<div class="entry-content-wrapper clearfix">
		<div style="padding:30px; background-color:#f4c733; border-radius:5px; " class="flex_column av_one_full  av-animated-generic pop-up  flex_column_div first  avia-builder-el-7  el_after_av_one_full  avia-builder-el-last  column-top-margin avia_start_animation avia_start_delayed_animation"><section itemtype="https://schema.org/CreativeWork" itemscope="itemscope" class="av_textblock_section"><div itemprop="text" class="avia_textblock "><p><a href="http://privilegiosfiscales.fundar.org.mx/wp-includes/creditos/PrivilegiosFin.pdf" class="alignnone" style="margin: 0px; padding: 0px; display: inline-block; position: relative; overflow: hidden;"><img height="200" width="1080" sizes="(max-width: 1080px) 100vw, 1080px" alt="32 veces el SAT ha incumplido resoluciones del INAI" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/descarga.png" class="alignnone size-full wp-image-94268 avia-image-container avia_animated_image avia_animate_when_almost_visible pop-up av-styling- avia-builder-el-8 avia-builder-el-no-sibling avia-align-center avia_start_animation avia_start_delayed_animation"><span class="image-overlay overlay-type-extern" style="left: -5px; top: 0px; overflow: hidden; display: block; height: 170px; width: 930px;"><span class="image-overlay-inside"></span></span></a></p>
</div></section></div>
		</div>
	 </div>
	 </div>
		  </div> <!-- /botón descarga investigación-->

		</main>

		<?php

		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();

		?>
	 </div>
			
	<script src="http://privilegiosfiscales.fundar.org.mx/wp-includes/js/graphs/d3.v3.min.js"></script>
	<script src="http://privilegiosfiscales.fundar.org.mx/wp-includes/js/graphs/barchart_script.js"></script>
	<script src="http://privilegiosfiscales.fundar.org.mx/wp-includes/js/graphs//jquery-3.1.0.min.js"></script>

	<script type="text/javascript"> 
	  var p = "10", sufix = "";
	  var base_url = "http://privilegiosfiscales.fundar.org.mx/wp-includes/js/graphs/"
	  if($( window ).width() < 760){
	    p = "90"; sufix = "_res"    
	  }

	  init(base_url + 'data_moral' + sufix + '.tsv', p, '#barChart')

	  $("#r1").on("click", function(){
	    init(base_url + 'data_moral' + sufix + '.tsv', p, '#barChart')
	    $("#r2").css({background: "transparent", color: "#2b3e59"})
	    $("#r1").css({background: "#2b3e59", color: "#fff"})
	  })

	  $("#r2").on("click", function(){
	    init(base_url + 'data_fisica' + sufix + '.tsv', p, '#barChart')
	    $("#r1").css({background: "transparent", color: "#2b3e59"})
	    $("#r2").css({background: "#2b3e59", color: "#fff"})
	  })

	</script> 


<?php get_footer(); ?>
