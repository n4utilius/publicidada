<?php 
	/*
	Template Name: Adeudos Condonados
	*/
	
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 	 get_header();

	 ?>
	 <style type="text/css">
	 	
		#pieChart{
		    display: block;
		    text-align: center;
		}

		.p0_title{
		    font-family: lato;
	    	font-size: 24px;
	    	font-weight: 900;
		}

		.acotaciones li {
			list-style: none;
			display:inline;
			margin:8px;
		}

		.acotaciones li span{
			padding: 5px;
		}

		.acotaciones li.red span{
			background: #AD1C1C;
			color: #AD1C1C;
		}

		.acotaciones li.blue span{
			background: #1F70A4;
			color: #1F70A4;
		}

	 </style>
	 
	 
	 
        <!-- cabecera-->
         <div class="avia-section-cabecera" id="av_section_hero-zone-conversion">
		<div class="container" style="padding: 0px;">
			<a class="cabecera-in" href="http://privilegiosfiscales.fundar.org.mx/"><img class="avia_image cancelados-logo" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/logo-cabecera2.png" itemprop="contentURL" /></a>
		</div>
	 </div><!-- /cabecera -->
	 
	 <div id="av_section_conversiones" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-20 el_after_av_section el_before_av_section container_wrap fullsize">
		
		<main style="margin-top: 0px; padding-top: 0px;" class='template-page content  <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>

                                <?php if (have_posts()) :
                                	while (have_posts()) : the_post(); ?>

		                                <article class='post-entry post-entry-type-page entry'>

			                                <div class="entry-content-wrapper clearfix">
                                                <?php
                                                echo '<div class="entry-content" '.avia_markup_helper(array('context' => 'entry_content','echo'=>false)).'>';
                                                    the_content(__('Read more','avia_framework').'<span class="more-link-arrow">  &rarr;</span>');
                                                echo '</div>';

                                                echo '<footer class="entry-footer">';
                                                wp_link_pages(array('before' =>'<div class="pagination_split_post">',
                                                                        'after'  =>'</div>',
                                                                        'pagelink' => '<span>%</span>'
                                        ));
                                                echo '</footer>';
                
                                                do_action('ava_after_content', get_the_ID(), 'page');
                                                ?>
		                                	</div>

		                                </article><!--end post-entry-->


                                <?php
	                                $post_loop_count++;
	                                endwhile;
	                                else:
                                ?>

                                    <article class="entry">
                                        <header class="entry-content-header">
                                            <h1 class='post-title entry-title'><?php _e('Nothing Found', 'avia_framework'); ?></h1>
                                        </header>

                                        <?php get_template_part('includes/error404'); ?>

                                        <footer class="entry-footer"></footer>
                                    </article>

                                <?php

	                                endif;
                                ?>
				
	                <!-- grafica-->
			<div id="av_section_1" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-0 avia-builder-el-no-sibling container_wrap fullsize" style="background-color: #edae44; background-color: #fff;">
			       <div class="container"">
				       <main class="template-page content av-content-full alpha units" itemprop="mainContentOfPage" role="main">
					     	<div class="areaChart">
								<div id="pieChart"> </div>
								<ul class="acotaciones">
									<li class="red"> <span>||||||</span> 36 Empresas </li>	
									<li class="blue">  <span>||||||</span> 41,363 Empresas </li>	
								</ul>
							</div>
				       </main>
			       </div>
			</div><!-- /grafica-->
			
	 <!-- botón descarga investigación-->
	 <div id="av_section_btnInvs" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-0 avia-builder-el-no-sibling container_wrap fullsize" style="background-color: #edae44; background-color: #fff;">
	 <div class="container"">
	 		<div class="post-entry post-entry-type-page">
		<div class="entry-content-wrapper clearfix">
		<div style="padding:30px; background-color:#f4c733; border-radius:5px; " class="flex_column av_one_full  av-animated-generic pop-up  flex_column_div first  avia-builder-el-7  el_after_av_one_full  avia-builder-el-last  column-top-margin avia_start_animation avia_start_delayed_animation"><section itemtype="https://schema.org/CreativeWork" itemscope="itemscope" class="av_textblock_section"><div itemprop="text" class="avia_textblock "><p><a href="http://privilegiosfiscales.fundar.org.mx/wp-includes/creditos/PrivilegiosFin.pdf" class="alignnone" style="margin: 0px; padding: 0px; display: inline-block; position: relative; overflow: hidden;"><img height="200" width="1080" sizes="(max-width: 1080px) 100vw, 1080px" alt="32 veces el SAT ha incumplido resoluciones del INAI" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/descarga.png" class="alignnone size-full wp-image-94268 avia-image-container avia_animated_image avia_animate_when_almost_visible pop-up av-styling- avia-builder-el-8 avia-builder-el-no-sibling avia-align-center avia_start_animation avia_start_delayed_animation"><span class="image-overlay overlay-type-extern" style="left: -5px; top: 0px; overflow: hidden; display: block; height: 170px; width: 930px;"><span class="image-overlay-inside"></span></span></a></p>
</div></section></div>
		</div>
	 </div>
	 </div>
		  </div> <!-- /botón descarga investigación-->		

		</main>

		<script src="http://privilegiosfiscales.fundar.org.mx/wp-includes/js/graphs/d3.v3.min.js"></script>
		<script src="http://privilegiosfiscales.fundar.org.mx/wp-includes/js/graphs/d3pie.min.js"></script>
		<script>
			var pie = new d3pie("pieChart", {
				"header": {
					"title": {
						"text": "Condonación de créditos fiscales",
						"fontSize": 24,
						"font": "open sans"
					},
					"subtitle": {
						"text": "Programa \"Ponte al Corriente\" 2013",
						"color": "#999999",
						"fontSize": 12,
						"font": "open sans"
					},
					"titleSubtitlePadding": 9
				},
				"footer": {
					"color": "#999999",
					"fontSize": 10,
					"font": "open sans",
					"location": "bottom-left"
				},
				"size": {
					"canvasWidth": (window.innerWidth < 760)? window.innerWidth: 700,
					"pieOuterRadius": "97%"
				},
				"data": {
					"sortOrder": "value-desc",
					"smallSegmentGrouping": {
						"enabled": true
					},
					"content": [
						{
							//"label": "36 empresas",
							"label": "$80,161",
							"value": 80161,
							"color": "#2484c1"
						},
						{
							//"label": "41,363 empresas",
							"label": "$79,549",
							"value": 79549,
							"color": "#cb2121"
						}
					]
				},
				"labels": {
					"outer": {
						"pieDistance": 32
					},
					"inner": {
						"hideWhenLessThanPercentage": 3
					},
					"mainLabel": {
						"fontSize": 11
					},
					"percentage": {
						"color": "#ffffff",
						"decimalPlaces": 0
					},
					"value": {
						"color": "#adadad",
						"fontSize": 11
					},
					"lines": {
						"enabled": true
					},
					"truncation": {
						"enabled": true
					}
				},
				/*
				"tooltips": {
					"enabled": true,
					"type": "placeholder",
					"string": "{label}: {value}, {percentage}%",
					"styles": {
						"borderRadius": 3,
						"fontSize": 18,
						"padding": 8
					}
				},
				*/
				"effects": {
					"pullOutSegmentOnClick": {
						"effect": "linear",
						"speed": 400,
						"size": 8
					}
				},
				"misc": {
					"gradient": {
						"enabled": true,
						"percentage": 100
					}
				},
				"callbacks": {}
			});
		</script>

		<?php

		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();

		?>
	 </div>

			

	



<?php get_footer(); ?>
