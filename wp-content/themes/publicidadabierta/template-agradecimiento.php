<?php 
	/*
	Template Name: Agradecimiento
	*/
	
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 	 get_header();

	 ?>
	 
	 
	 
        <!-- cabecera-->
         <div class="avia-section-cabecera" id="av_section_hero-zone-conversion">
		<div class="container" style="padding: 0px;">
			<a class="cabecera-in" href="http://privilegiosfiscales.fundar.org.mx/"><img class="avia_image cancelados-logo" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/logo-cabecera2.png" itemprop="contentURL" /></a>
		</div>
	 </div><!-- /cabecera -->
	 
	 <div id="av_section_conversiones" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-20 el_after_av_section el_before_av_section container_wrap fullsize">
		
		<main style="margin-top: 0px; padding-top: 0px;" class='template-page content  <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>

                                <?php if (have_posts()) :
                                	while (have_posts()) : the_post(); ?>

		                                <article class='post-entry post-entry-type-page entry'>

			                                <div class="entry-content-wrapper clearfix">
                                                <?php
                                                echo '<div class="entry-content" '.avia_markup_helper(array('context' => 'entry_content','echo'=>false)).'>';
                                                    the_content(__('Read more','avia_framework').'<span class="more-link-arrow">  &rarr;</span>');
                                                echo '</div>';

                                                echo '<footer class="entry-footer">';
                                                wp_link_pages(array('before' =>'<div class="pagination_split_post">',
                                                                        'after'  =>'</div>',
                                                                        'pagelink' => '<span>%</span>'
                                        ));
                                                echo '</footer>';
                
                                                do_action('ava_after_content', get_the_ID(), 'page');
                                                ?>
		                                	</div>

		                                </article><!--end post-entry-->


                                <?php
	                                $post_loop_count++;
	                                endwhile;
	                                else:
                                ?>

                                    <article class="entry">
                                        <header class="entry-content-header">
                                            <h1 class='post-title entry-title'><?php _e('Nothing Found', 'avia_framework'); ?></h1>
                                        </header>

                                        <?php get_template_part('includes/error404'); ?>

                                        <footer class="entry-footer"></footer>
                                    </article>

                                <?php

	                                endif;
                                ?>
					
		</main>

		<?php

		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();

		?>
	 </div>

		<!-- Google Code for suscripcion a privilegios fiscales Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963761815;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "BWAzCNbx1moQl63HywM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/963761815/?label=BWAzCNbx1moQl63HywM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
	

	



<?php get_footer(); ?>
