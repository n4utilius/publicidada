<?php
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config, $more;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 get_header();
	
		
		$showheader = true;
		if(avia_get_option('frontpage') && $blogpage_id = avia_get_option('blogpage'))
		{
			if(get_post_meta($blogpage_id, 'header', true) == 'no') $showheader = false;
		}
		
	 	if($showheader)
	 	{
			echo avia_title(array('title' => avia_which_archive()));
		}
		
		do_action( 'ava_after_main_title' );
	
	
	
	
	?>
	<?php 
	$data = getDataSentencias();
	//print_r($data);
	$selectedTipo_persona = getParameterValueGET('tipo_persona');
    $selectedTipo_credito = getParameterValueGET('tipo_credito');
    $selectedEstado = getParameterValueGET('estado');
    $selectedCategoria = getParameterValueGET('categoria');



?>
		



        <!-- cabecera-->
         <div class="avia-section-cabecera" id="av_section_hero-zone-conversion">
		<div class="container" style="padding: 0px;">
			<a class="cabecera-in" href="http://privilegiosfiscales.fundar.org.mx/"><img class="avia_image cancelados-logo" src="http://privilegiosfiscales.fundar.org.mx/wp-content/uploads/2016/09/logo-cabecera2.png" itemprop="contentURL" /></a>
		</div>
	 </div><!-- /cabecera -->
			
			
			
			
<!-- Fin filtros iniciativas -->		
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<style>
theader input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
	tfoot {
    display: table-header-group;
}
.down a {
  display: block;
  height: 50px;
  width: 40%;
  width: 200px;
  background: #00b7ea;
  
  /*TYPE*/
  color: white;
 
  text-decoration: none;
  text-align: center;
  text-transform: uppercase;
}
</style>

	<script type="text/javascript" class="init">
	
$(document).ready(function() {
	$('#example').DataTable( {
		"ajax": "http://privilegiosfiscales.fundar.org.mx/wp-includes/css/creditos2.txt",
		'columns': [
       null,
       null,
       null,
       null,
       null,
	   null,
       null,
       null,
       null,
       { 
         render: function(data, type, row, meta){
            if(type === 'display'){
                var abbr = "MX";
                var symbol = "$";              
                 
               var num = $.fn.dataTable.render.number(',', '.', 2, symbol).display(data);              
               return num + ' ';           
              
            } else {
               return data;
            }
         }
       }
     ],
		"order": [[ 9, "desc" ]],
	   "columnDefs": [
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 0 ],
                "visible": false,
				"searchable": false
            },
			 
        ],
	   "language": {
	   "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
	"oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
        }
	}


	);
	$('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Busqueda.." />' );
    } );
	

    var table = $('#example').DataTable();
	
	table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
  } );
	
} )	;

	
</script>


	<div class="container_wrap container_wrap_first main_color fullsize" style="margin: 70px 0px;">

<div class="down">
<a href="http://privilegiosfiscales.fundar.org.mx/wp-includes/creditos/BDcreditos.csv">Descargar Base de datos</a>
</div>
	<div class='container'>



		<main class='template-page content  av-content-full alpha units'>
					<!--fixed search -->
												
												<aside id="buscadorddd" class="dddbuscador-fijo">
													<div class="cointeinerddd" data-track-id="">

												<?php 
													$data = getDataSentencias();
													//print_r($data);
													$selectedTipo_persona = getParameterValueGET('tipo_persona');
													$selectedTipo_credito = getParameterValueGET('tipo_credito');
													$selectedEstado = getParameterValueGET('estado');
													$selectedCategoria = getParameterValueGET('categoria');
													$selectedMontos = getParameterValueGET('montos');
													?>

													<div class="box-menu">
														<div class="search-table">
															<form name="filter-materia" id="filter-materia" action="/privilegiados/">
																<div class="col span_4 columns clear-both">  
																	<div id="BuscarSentencia">				
																	<h1>
																		<input list="etiquetados" type="text" placeholder="" id="jose" name="search_query" value="" class="ui-autocomplete-input" autocomplete="on"></h1> 														  
																		
																	</div>
																</div>
																
																
																
																
																	<div class="col span_3 one-fourths clear-both"> 
																<div id="filter">				
																	<select class="sorter-rep sort" name="tipo_credito" id="tipo-filter">
																		<option value="">Tipo de Credito</option>
																		<?php $Tipo_creditosArray = gettipo_creditos(); ?>
																		<?php foreach($Tipo_creditosArray as $value) { ?>
																		<option value="<?php echo utf8_encode($value["slug"]);?>" <?php if($selectedTipo_credito == utf8_encode($value["slug"])) echo 'selected="selected"'?>>
																					 <?php echo utf8_encode($value["name"]);?>
																		</option>
																		<?php } ?>
																	</select>
																</div>
																</div>
																
																<div class="col span_3 one-fourths clear-both"> 
																<div id="filter">				
																	<select class="sorter-rep sort" name="tipo_persona" id="tipo-filter">
																		<option value="">Tipo de Persona</option>
																		<?php $Tipo_personaArray = gettipo_personas(); ?>
																		<?php foreach($Tipo_personaArray as $value) { ?>
																		<option value="<?php echo utf8_encode($value["slug"]);?>" <?php if($selectedTipo_persona == utf8_encode($value["slug"])) echo 'selected="selected"'?>>
																					 <?php echo utf8_encode($value["name"]);?>
																		</option>
																		<?php } ?>
																	</select>
																</div>
																</div>

															
																
																	
																<div class="col span_3 columns clear-both"> 					   
																<div id="filter">				
																	<select class="sorter-rep sort" name="estado" id="estado-filter">
																		<option value="">Estado</option>
																		<?php $EstadosArray = getestados(); ?>
																		<?php foreach($EstadosArray as $value) { ?>
																		<option value="<?php echo utf8_encode($value["slug"]);?>" <?php if($selectedEstado == utf8_encode($value["slug"])) echo 'selected="selected"'?>>
																			<?php echo utf8_encode($value["name"]);?>
																		</option>
																		<?php } ?>
																	</select>
																</div>
																</div>
																
																<div class="col span_3 columns clear-both clear-both">	   					   
																<div id="filter">				
																	<select class="sorter-rep sort" name="categoria" id="categoria-filter">
																		<option value="">Categorias</option>
																		<?php $categoriasArray = getcategorias(); ?>
																		<?php foreach($categoriasArray as $value) { ?>
																		<option value="<?php echo utf8_encode($value["slug"]);?>" <?php if($selectedCategoria == utf8_encode($value["slug"])) echo 'selected="selected"'?>>
																			<?php echo utf8_encode($value["name"]);?>
																		</option>
																		<?php } ?>
																	</select>
																</div>				
																</div>
																
																<div class="col span_3 columns clear-both clear-both">	   					   
																<div id="filter">				
																	<select class="sorter-rep sort" name="montos" id="montos-filter">
																		<option value="">Montos</option>
																		<?php $montosArray = getmontos(); ?>
																		<?php foreach($montosArray as $value) { ?>
																		<option value="<?php echo utf8_encode($value["slug"]);?>" <?php if($selectedMontos == utf8_encode($value["slug"])) echo 'selected="selected"'?>>
																			<?php echo utf8_encode($value["name"]);?>
																		</option>
																		<?php } ?>
																	</select>
																</div>				
																</div>
																
																
																
																
																<div class="col span_1 col_last">					   
																				
																	<input type="submit" value="Filtrar" id="submit-filter"/>
																
																</div>
																		
															</form>								   
														</div>
													</div>



													</div>
												</aside>


			                 <?php if($data) { ?>
							 
							<?php echo "Se encontraron: <b>". $data->found_posts. " </b>resultados<br>" ; ?>
						    <?php if($_GET["search_query"] != "") { echo "Por la palabra: <b>". $_GET["search_query"]."</b><br>"; }?>
							<?php if($_GET["tipo_persona"] != "") {  echo "Por filtro Tipo de persona: <b>" .($_GET["tipo_persona"])."</b><br>";} ?>
							<?php if($_GET["tipo_credito"] != "") {  echo "Por filtro Tipo de Credito: <b>" .($_GET["tipo_credito"])."</b><br>";} ?>
							<?php if($_GET["estado"] != "") {  echo "Por filtro Estado: <b>" . estadosd($_GET["estado"])."</b><br>";} ?>
							<?php if($_GET["categoria"] != "") {  echo "Por filtro Categoria: <b>" .slugreverse($_GET["categoria"])."</b><br>";} ?>
							 
							 
					 <?php if ($data->have_posts()) { ?>
					 <?php while ($data->have_posts()) : $data->the_post(); ?>
							
					<div class="article-content-wrap">
						<div class="post-sentencias-box">
							<div class="izquierda">
								 						    
								 <?php $tipo_persona = get_post_meta($post->ID, 'tipo_persona', true); ?>
								 <?php echo "<br> Razón Social: <b>". get_post_meta($post->ID, 'razon_social', true)."</b>"; ?>
								 <?php echo "&nbsp; &nbsp; &nbsp; RFC: ". get_post_meta($post->ID, 'rfc', true); ?>
								 <?php echo "<br> Categoría: ". get_post_meta($post->ID, 'categoria', true); ?>
								<?php echo "&nbsp; &nbsp; &nbsp; Subcategoria: ". get_post_meta($post->ID, 'subcategoria', true); ?>
								<?php echo "<br> Tipo de credito: ". get_post_meta($post->ID, 'tipo_credito', true); ?>								
								<?php echo "&nbsp; &nbsp; &nbsp;  Tipo de Persona: ". $tipo_persona;?>
								<?php $monto= get_post_meta($post->ID, 'monto_sat', true);?>
								<?php echo "<br> Monto del credito: $ ".number_format ($monto);?>
								<?php $nestado = get_post_meta($post->ID, 'estado', true); ?> 
								<?php echo "<br> Estado: " .estadosd($nestado) ; ?>

								</div>
						 </div>				
					</div><!--article-content-wrap-->
														
                                        <?php endwhile; ?>
					<?php } else { ?>
					<p>No se encontraron iniciativas con esta busqueda</p>
					<?php } ?>
					<?php
						if($data) {
							echo  avia_pagination2('', 'nav', $data);
						} else {
							echo avia_pagination2('', 'nav');
						}
						
						
					?>
					
					<?php } else { ?>

							
					<!--fin iniciativas-->
							
													
									<div class="col span_12">
					<div class="post-sentencias-box">
						  <div class="izquierda">
							  	 						    
								
							
						  </div>							  
						  <div class="post-header info_sentencias">
							   							    
							   
							   <body class="wide comments example">
									<div class="fw-container">
								<div class="fw-header fh-fixedHeader">
									<div class="nav-master">
										<div class="account"></div>
									</div>
								
								</div>
								<div class="fw-nav">
									<div class="nav-main">
									</div>
								</div>
								<div class="fw-body">
									<div class="content">
											<div class="info">
										</div>
										<table id="example" class="display" cellspacing="0" width="100%">
											<thead>
												<tr>
										<th>ID</th>
										<th>Tipo de persona</th>
										<th>Estado</th>
										<th>categoria</th>
										<th>Subcategoria</th>
										<th>Razon Social</th>
										<th>Rfc</th>
										<th>Tipo Credito</th>
										<th>Monto Sat</th>
										<th>Monto</th>
												</tr>
									
											</thead>
										<tfoot>
												<tr>
										<th>ID</th>
										<th>Tipo de persona</th>
										<th>Estado</th>
										<th>Categoria</th>
										<th>Subcategoria</th>
										<th>Razon Social</th>
										<th>Rfc</th>
										<th>Tipo de Credito</th>
										<th>Monto Sat</th>
										<th>Monto</th>
												</tr>
											</tfoot>		
										</table>
										<div class="tabs">
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</body>
								
							  
							  
						  </div><!--/post-header-->
					</div>	  
							
				</div><!--/span_12-->	
							
				
					<?php } ?>
							
			
			
			<!----adddddddddddddddddddddddddd -->
			
				
					
		</div><!--end container-->
	</div><!-- close default .container_wrap element -->



<?php get_footer(); ?>
