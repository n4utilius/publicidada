<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/
global $avia_config;

function privilegiados_init() {
    $args = array(
      'label' => 'Privilegiados',
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => array('slug' => 'privilegiados'),
        'query_var' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-portfolio',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',)
        );
    register_post_type( 'privilegiados', $args );
}
add_action( 'init', 'privilegiados_init' );



function getDataSentencias() {
	$meta_query = false;
	
	/*values form*/
		$where = Array();
		$array = Array();
	
	if(isset($_GET["search_query"]) and $_GET["search_query"] != "") {
			/*para buscar palabra por palabra*/
			$arraysearch_query = explode("+",($_GET["search_query"]));
			$wherequery = "";
			foreach($arraysearch_query as $value) {
				 $wherequery .=$value;

			}
			 $meta_query[] = getTodo($wherequery);

		}
		
	
	if(isset($_GET["tipo_persona"]) and $_GET["tipo_persona"] != "") {
		$meta_query[] = getSentenciasBytipo_persona($_GET["tipo_persona"]);
	}
	if(isset($_GET["estado"]) and $_GET["estado"] != "") {
		$meta_query[] = getSentenciasByestado($_GET["estado"]);
	}	
	if(isset($_GET["categoria"]) and $_GET["categoria"] != "") {
		$meta_query[] = getSentenciasBycategoria($_GET["categoria"]);
	}
	if(isset($_GET["tipo_credito"]) and $_GET["tipo_credito"] != "") {
		$meta_query[] = getSentenciasBytipo_credito($_GET["tipo_credito"]);
	}	
	if(isset($_GET["montos"]) and $_GET["montos"] != "") {
		$meta_query[] = getSentenciasBymontos($_GET["montos"]);
	}	
	
	
	
	if($meta_query) {
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args  = array(
			'post_type' => 'privilegiados',
			'posts_per_page' => 8,
			'paged' => $paged,
			'orderby' => 'title', 'order' => 'ASC',
			'meta_query' => $meta_query
		);
		$loop = new WP_Query($args);
		
		return $loop;
	} else {
		return false;
	}
}
		/*get data by parameter $_GET */
	function getParameterValueGET($var = "") {
		if(isset($_GET[$var])) {
			return $_GET[$var];
		} else {
				return "";
			}
		}	
	function getSentenciasBytipo_persona($tipo_persona) {
		return array('key' => 'tipo_persona', 'value' => $tipo_persona, 'compare' => 'LIKE' );
	}
	function getSentenciasBytipo_credito($tipo_credito) {
		return array('key' => 'tipo_credito', 'value' => $tipo_credito, 'compare' => 'LIKE' );
	}
	function getSentenciasBymontos($montos) {
		return array('key' => 'monto_sat', 'type' => 'decimal', 'value' => $montos, 'compare' => 'BETWEEN' );
	}
	
	
	function getSentenciasByestado($estado) {
		$array = explode("|",$estado);
		$clave = array_search($estado, $array);
	    $array[$clave];
		
		
		return array('key' => 'estado', 'value' => $array[$clave], 'compare' => 'LIKE' );
	
	}
	
	function getSentenciasBycategoria($categoria) {
		$array = explode("|",$categoria);
		$clave = array_search($categoria, $array);
	    $array[$clave];
		
		
		return array('key' => 'categoria', 'value' => $array[$clave], 'compare' => 'LIKE' );
	
	
	
	}


	function getTodo($string) {
	
		return array( 'value' => $string, 'compare' => 'LIKE' );
	}
	
	
	/*Get tipo_personas array*/
	function gettipo_personas() {
		$tipo_personas = array(
			array("name" => "Personas Fisicas", "slug" => "fisica"),
			array("name" => "Personas Morales",  "slug" => "moral"),
			
		);
		
		return $tipo_personas;
	}
	
	
	
	/*Get tipo_creditos array*/
	function gettipo_creditos() {
		$tipo_creditos = array(
			array("name" => "Creditos Condonados", "slug" => "condonado"),
			array("name" => "Creditos Cancelados",  "slug" => "cancelado"),
			
		);
		
		return $tipo_creditos;
	}
	
	/*Get tipo_montos array*/
	function getmontos() {
		$montos = array(
			array("name" => "0 a 100 mil", "slug" => "0,100000"),
			array("name" => "100 mil a 1 millon",  "slug" => "100001, 1000000"),
			array("name" => "1 millon a  10 millones",  "slug" => "1000001, 10000000"),
			array("name" => "10 millones a 100 millones",  "slug" => "10000001, 100000000"),
			array("name" => "mas de 100 millones",  "slug" => "100000001, 100000000000"),
		);
		
		return $montos;
	}


	function getestados() {
		$estados = array(
			array("name" => "Aguascalientes", "slug" => "AGS"),
			array("name" => "Baja California Norte", "slug" => "BCN"),
			array("name" => "Baja California Sur", "slug" => "BCS"),			
			array("name" => "Campeche", "slug" => "CAMP"),
			array("name" => "Ciudad de Mexico", "slug" => "TODAS"),
			array("name" => "Chiapas", "slug" => "CHIS"),
			array("name" => "Chihuahua", "slug" => "CHIH"),	
			array("name" => "Coahuila", "slug" => "COAH"),
			array("name" => "Colima", "slug" => "COL"),		
			array("name" => "Durango", "slug" => "DGO"),
			array("name" => "Guanajuato", "slug" => "GTO"),
			array("name" => "Guerrero", "slug" => "GRO"),			
			array("name" => "Hidalgo", "slug" => "HGO"),	
			array("name" => "Jalisco", "slug" => "JAL"),
			array("name" => "Mexico", "slug" => "MEX"),
			array("name" => "Michoacan", "slug" => "MICH"),
			array("name" => "Morelos", "slug" => "MOR"),
			array("name" => "Nayarit", "slug" => "NAY"),
			array("name" => "Nuevo Leon", "slug" => "NL"),
			array("name" => "Oaxaca", "slug" => "OAX"),
			array("name" => "Queretaro", "slug" => "QRO"),
			array("name" => "Quintanarro", "slug" => "QROO"),
			array("name" => "San luis potosi", "slug" => "SLP"),
			array("name" => "Sinaloa", "slug" => "SIN"),
			array("name" => "Sonara", "slug" => "SON"),
			array("name" => "Tabasco", "slug" => "TAB"),
			array("name" => "Tamaulipas", "slug" => "TAM"),
			array("name" => "Tlaxcala", "slug" => "TLAX"),			
			array("name" => "Veracruz", "slug" => "VER"),
			array("name" => "Yucatan", "slug" => "YUC"),
			array("name" => "Zacatecas", "slug" => "ZAC"),
	);
		
		return $estados;
	}

function getcategorias() {
		$categorias = array(
			array("name" => "Construcci&oacute;n", "slug" => "construccion"),
			array("name" => "Servicios",  "slug" => "servicios"),
			array("name" => "Transporte", "slug" => "transporte"),
			array("name" => "Comercio al por menor", "slug" => "comercio-al-por-menor"),
			array("name" => "Fabricaci&oacute;n, producci&oacute;n o elaboraci&oacute;n", "slug" => "fabricacion-producion-o-elaboracion"),
			array("name" => "Comercio al por mayor", "slug" => "comercio-al-por-mayor"),
			array("name" => "Recursos Naturales", "slug" => "recursos-naturales"),
			array("name" => "Gobierno", "slug" => "gobierno"),
			array("name" => "Otros Ingresos", "slug" => "otros-ingresos"),
			array("name" => "Comunicaci&oacute;n", "slug" => "comunicacion"),
			
		);
		
		return $categorias;
	}

function slugreverse($string) {		
	$characters = array(
		"Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u", 
		"á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
		"à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u", "ã" => "a", "¿" => "", 
		"?" =>  "", "¡" =>  "", "!" =>  "", ": " => "-", "ü" => "u"
	);
	
	$string = strtr($string, $characters); 
	$string = ucwords(trim($string));
	$string = preg_replace("/-+/", " ", $string);
	
	if(substr($string, strlen($string) - 1, strlen($string)) === " ") {
		$string = substr($string, 0, strlen($string) - 1);
	}
	
	return $string;
}

function estadosd($string) {		
	$characters = array(
		"COL" => "Colima", "TAB" => "Tabasco", "VER" => "Veracruz", "NL" => "Nuevo Leon", "GTO" => "Guanajuato", "MEX" => "Mexico", "QRO" => "Queretaro", 
		"CHIH" => "Chihuahua", "ZAC" => "Zacatecas", "TAM" => "Tamaulipas", "OAX" => "Oaxaca", "SON" => "Sonora", "SIN" => "Sinaloa", "QROO" => "Quintanaroo",
		"AGS" => "Aguascalientes", "JAL" => "Jalisco", "SLP" => "San Luis potosi", "YUC" => "Yucatan", "NAY" => "Nayarit", "COAH" => "Coahuila", "BCN" => "Baja California Norte", 
		"MICH" =>  "Michoacan", "DGO" =>  "Durnago", "HGO" =>  "Hidalgo", "CHIS" => "Chiapas", "MOR" => "Morelia", "CAMP" => "Campeche", "GRO" => "Guerrero", "MOR" => "Morelos", "BCS" => "Baja Calfornia Sur","TODAS" => "Ciudad de Mexico", "TLAX" => "Tlaxcala");
	
	$string = strtr($string, $characters); 
	$string = ucwords(trim($string));
	$string = preg_replace("/-+/", " ", $string);
	
	if(substr($string, strlen($string) - 1, strlen($string)) === " ") {
		$string = substr($string, 0, strlen($string) - 1);
	}
	
	return $string;
}

function avia_pagination2($pages = '', $wrapper = 'div', $custom = false) {
	global $paged;

	if(get_query_var('paged')) {
	     $paged = get_query_var('paged');
	} elseif(get_query_var('page')) {
	     $paged = get_query_var('page');
	} else {
	     $paged = 1;
	}

	$output = "";
	$prev = $paged - 1;
	$next = $paged + 1;
	$range = 2; // only edit this if you want to show more page-links
	$showitems = ($range * 2)+1;



	if($pages == '') {
		if($custom) {
			$pages = $custom->max_num_pages;
			if(!$pages) {
				$pages = 1;
			}
		} else {
			global $wp_query;
			//$pages = ceil(wp_count_posts($post_type)->publish / $per_page);
			$pages = $wp_query->max_num_pages;
			if(!$pages) {
				$pages = 1;
			}
		}
		
	}

	$method = "get_pagenum_link";

	if(1 != $pages) {
		$output .= "<$wrapper class='pagination'>";
		$output .= "<span class='pagination-meta'>".sprintf(__("Pagina  %d de %d"), $paged, $pages)."</span>";
		$output .= ($paged > 2 && $paged > $range+1 && $showitems < $pages)? "<a href='".$method(1)."'>&laquo;</a>":"";
		$output .= ($paged > 1 && $showitems < $pages)? "<a href='".$method($prev)."'>&lsaquo;</a>":"";


		for($i=1; $i <= $pages; $i++) {
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
				$output .= ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".$method($i)."' class='inactive' >".$i."</a>";
			}
		}

		$output .= ($paged < $pages && $showitems < $pages) ? "<a href='".$method($next)."'>&rsaquo;</a>" :"";
		$output .= ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) ? "<a href='".$method($pages)."'>&raquo;</a>":"";
		$output .= "</$wrapper>\n";
	}

	return $output;
}


