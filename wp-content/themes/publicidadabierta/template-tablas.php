<?php 
	/*
	Template Name: Tablas
	*/
	
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 get_header();  
?>
<?php
header("Access-Control-Allow-Origin: *");
 ?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<style>
theader input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
	tfoot {
    display: table-header-group;
}
</style>

	<script type="text/javascript" class="init">
	
$(document).ready(function() {
	$('#example').DataTable( {
		"ajax": "http://privilegiosfiscales.fundar.org.mx/wp-includes/css/creditos2.txt",
		"order": [[ 8, "desc" ]],
	   "columnDefs": [
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 0 ],
                "visible": false,
				"searchable": false
            }
        ],
		 "language": {
	   "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
	"oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    } 
					}
        
	} );
	$('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="" />' );
    } );
	

    var table = $('#example').DataTable();
	
	table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
  } );
	
} )	;

	
</script>
	        	
		<div class='section-cabecera'>     
				<div class="container" style="padding: 10px 0px 5px;">
                                        <div id="escaparate" class="mobile-int">
						<a class="cabecera-in" href="http://privilegiosfiscales.fundar.org.mx/"><img class="logo-in" src="/wp-content/themes/privilegios/images/logo-privilegios-in.png"></a>
                                        </div>
				</div>	
		</div>
		<div class='container_wrap container_wrap_first main_color <?php avia_layout_class( 'main' ); ?>'>

			<div class='container'>

				<main class='template-page content  <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>
			


									<body class="wide comments example">
									<div class="fw-container">
								<div class="fw-header fh-fixedHeader">
									<div class="nav-master">
										<div class="account"></div>
									</div>
								
								</div>
								<div class="fw-nav">
									<div class="nav-main">
									</div>
								</div>
								<div class="fw-body">
									<div class="content">
											<div class="info">
										</div>
										<table id="example" class="display" cellspacing="0" width="100%">
											<thead>
												<tr>
										<th>ID</th>
										<th>Tipo de persona</th>
										<th>Estado</th>
										<th>categoria</th>
										<th>Subcategoria</th>
										<th>Razon Social</th>
										<th>Rfc</th>
										<th>Monto Scrap</th>
										<th>Monto Sat</th>
										<th>Tipo Credito</th>
												</tr>
									
											</thead>
										<tfoot>
												<tr>
										<th>ID</th>
										<th>Tipo de persona</th>
										<th>Estado</th>
										<th>Categoria</th>
										<th>Subcategoria</th>
										<th>Razon Social</th>
										<th>Rfc</th>
										<th>Monto Scrap</th>
										<th>Monto Sat</th>
										<th>Tipo Credito</th>
												</tr>
											</tfoot>
										</table>
										<div class="tabs">
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</body>

				
				
				

				<!--end content-->
				</main>

				
			</div><!--end container-->

		</div><!-- close default .container_wrap element -->



<?php get_footer(); ?>
