<?php 
	/*
	Template Name: Conversion
	*/
	
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 	 get_header();


 	 if( get_post_meta(get_the_ID(), 'header', true) != 'no') echo avia_title();
 	 
 	 do_action( 'ava_after_main_title' );
	 
	
	 $tuitkey=rand(0,31);
	 $tuits=array(
	 	"@CarlosLozanoAgs",
		"@KIKOVEGA_",
		"@cmendozadavis",
		"@alitomorenoc",
		"@VelascoM_",
		"@Javier_Corral",
		"@rubenmoreiravdz",
		"@nachoperaltacol",
		"@ManceraMiguelMX",
		"@AispuroDurango",
		"@eruviel_avila",
		"@omarfayad",
		"@miguelmarquezm",
		"@HectorAstudillo",
		"@AristotelesSD",
		"@Silvano_A",
		"@gracoramirez",
		"@RobertoSandoval",
		"@JaimeRdzNL",
		"@GabinoCue",
		"@RafaMorenoValle",
		"@PanchDominguez",
		"@CarlosJoaquin",
		"@JMCarrerasGob",
		"@malovamx",
		"@claudiapavlovic",
		"@nunezarturo",
		"@fgcabezadevaca",
		"@GobTlaxcala",
		"@GobiernoVer",
		"@RolandoZapataB",
		"@ATelloC"
	);

	 $pretuit= "." . $tuits[$tuitkey] . " únete al reto. Publica el gasto en publicidad oficial con esta herramienta http://bit.ly/2gjaQPx";
	 $tuit= "https://twitter.com/intent/tweet?hashtags=PublicidadAbierta&text=" . urlencode($pretuit);
	 
	 
	 ?> 
	 
	 
	 <!-- zona hero conversion -->
	 <div class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll  avia-builder-el-0  el_before_av_section  avia-builder-el-first  av-minimum-height av-minimum-height-100 container_wrap fullsize" id="av_section_hero-zone-conversion">
		<div class="container" style="margin-top: 0px; padding-top:25px;">
			<main class="template-page content av-content-full alpha units" role="main" style="padding: 0px;">		
					<div class="entry-content-wrapper clearfix">
					        
						 <img class="brand" src="wp-content/themes/publicidad-abierta/images/logo-svg.svg" />
						 <h1 id="intro">En 2015, el gasto federal en publicidad oficial ascendió a más de 9 mil 619 millones de pesos, 248 veces mayor que el operado por el programa de Defensa de los Derechos Humanos el mismo año.
</h1>
						 <p id="sub-intro">¿Y cuánto se gastan en las entidades federativas? No se sabe. La mitad de los estados no informan a detalle sobre estos gastos. 
</p>
					        
						<div style="border-radius:0px; " class="flex_column av_one_half  flex_column_div av-zero-column-padding first  avia-builder-el-1  el_before_av_one_half  avia-builder-el-first  ">
							<section itemtype="https://schema.org/CreativeWork" itemscope="itemscope" class="av_textblock_section" id="tweet_area">
								<div itemprop="text" style="font-size:24px; " class="avia_textblock ">
		<img class="img-hero" src="wp-content/themes/publicidad-abierta/images/bird.svg" />
		<p class="resumen-hero-converversion">Envía un tuit a tu gobernador utilizando  <b style="color: black; font-weight: 600;">#PublicidadAbierta</b> para que se sume a la herramienta transparencia en publicidad oficial.</p>
									<p class="tw-texto"><?php echo $pretuit; ?></p>
									<!--p class="tw-texto"></p-->
									<div class="flex_column av_one_fourth first">
									<img class="clue" src="wp-content/themes/publicidad-abierta/images/arrow.svg" />
									</div>
									<!--botón -->
									<div class="flex_column av_three_fourth el_after_av_one_half" style="padding: 0px; margin: 0px; width: 79%;">
										<select id="state_choise" class="__web-inspector-hide-shortcut__">
								      <option value=""> Selecciona un estado </option><option value="Aguascalientes">Aguascalientes</option><option value="Baja California">Baja California</option><option value="Baja California Sur">Baja California Sur</option><option value="Campeche">Campeche</option><option value="Chiapas">Chiapas</option><option value="Chihuahua">Chihuahua</option><option value="Coahuila">Coahuila</option><option value="Colima">Colima</option><option value="Ciudad de México">Ciudad de México</option><option value="Durango">Durango</option><option value="México">México</option><option value="Hidalgo">Hidalgo</option><option value="Guanajuato">Guanajuato</option><option value="Guerrero">Guerrero</option><option value="Jalisco">Jalisco</option><option value="Michoacán">Michoacán</option><option value="Morelos">Morelos</option><option value="Nayarit">Nayarit</option><option value="Nuevo León">Nuevo León</option><option value="Oaxaca">Oaxaca</option><option value="Puebla">Puebla</option><option value="Querétaro">Querétaro</option><option value="Quintana Roo">Quintana Roo</option><option value="San Luis Potosí">San Luis Potosí</option><option value="Sinaloa">Sinaloa</option><option value="Sonora">Sonora</option><option value="Tabasco">Tabasco</option><option value="Tamaulipas">Tamaulipas</option><option value="Tlaxcala">Tlaxcala</option><option value="Veracruz">Veracruz</option><option value="Yucatán">Yucatán</option><option value="Zacatecas">Zacatecas</option>
								    </select>
									</div>

									<div class="flex_column av_three_fourth el_after_av_one_half" style="padding: 0px; margin: 0px; width: 79%;">
									<a class="avia-button-2" id="enviar_tweet" href="<?php echo $tuit; ?>">Manda el mensaje</a>
									</div>
									

								</div>
							</section>
						</div>
						<div style="border-radius:0px; " class="flex_column av_one_half  flex_column_div av-zero-column-padding   avia-builder-el-4  el_after_av_one_half  avia-builder-el-last  ">
							<div itemtype="https://schema.org/ImageObject" itemscope="itemscope" class="avia_animated_image avia_animate_when_almost_visible pop-up av-styling-no-styling   avia-builder-el-5  avia-builder-el-no-sibling  avia-align-right  avia_start_animation avia_start_delayed_animation">
								<div class="avia-image-container-inner">
									<img alt="" src="wp-content/themes/publicidad-abierta/images/publicidad-abierta.svg" class="avia_image ilustracion-hero">
								</div>
							</div>
						</div>
					</div>
			</main><!-- close content main element -->
		</div>
		<div class="av-extra-border-element border-extra-diagonal border-extra-diagonal-inverse "><div class="av-extra-border-outer"><div class="av-extra-border-inner" style="background-color:#fff;"></div></div></div>
	 </div><!-- /zona hero conversion -->
	
	 
	 <div id="av_section_conversiones" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll avia-builder-el-20 el_after_av_section el_before_av_section container_wrap fullsize">
		
		<main style="margin-top: 0px; padding-top: 0px;" class='template-page content  <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>

                                <?php if (have_posts()) :
                                	while (have_posts()) : the_post(); ?>

		                                <article class='post-entry post-entry-type-page entry'>

			                                <div class="entry-content-wrapper clearfix">
                                                <?php
                                                echo '<div class="entry-content" '.avia_markup_helper(array('context' => 'entry_content','echo'=>false)).'>';
                                                    the_content(__('Read more','avia_framework').'<span class="more-link-arrow">  &rarr;</span>');
                                                echo '</div>';

                                                echo '<footer class="entry-footer">';
                                                wp_link_pages(array('before' =>'<div class="pagination_split_post">',
                                                                        'after'  =>'</div>',
                                                                        'pagelink' => '<span>%</span>'
                                        ));
                                                echo '</footer>';
                
                                                do_action('ava_after_content', get_the_ID(), 'page');
                                                ?>
		                                	</div>

		                                </article><!--end post-entry-->


                                <?php
	                                $post_loop_count++;
	                                endwhile;
	                                else:
                                ?>

                                    <article class="entry">
                                        <header class="entry-content-header">
                                            <h1 class='post-title entry-title'><?php _e('Nothing Found', 'avia_framework'); ?></h1>
                                        </header>

                                        <?php get_template_part('includes/error404'); ?>

                                        <footer class="entry-footer"></footer>
                                    </article>

                                <?php

	                                endif;
                                ?>

		</main>

		<?php

		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();

		?>
	 </div>

			
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCrc1M8U4_5ZaCavrEreDpkJsXhmai7Jbg"></script> 
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/create_tweet.js"></script>

<script type="text/javascript"> 
  $("body").ready(function(){

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
    } else{
      errorFunction()
    }

    $(document).on('change', '#state_choise', function() {
      if(this.value && this.value != "" ){
        print_tweet(this.value)
      }
    });

    $(".avia-button-2#enviar_tweet").on("click", function(){
      window.location.href = "http://publicidadabierta.fundar.org.mx/agradecimiento/"
    })


    window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);
     
      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };
     
      return t;
    }(document, "script", "twitter-wjs"));
  })

</script> 
	

<!-- Google Code for suscripcion a privilegios fiscales Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963761815;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "BWAzCNbx1moQl63HywM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/963761815/?label=BWAzCNbx1moQl63HywM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<?php get_footer(); ?>
