  var CreateTweet = function(state, hashtag){
    this.hashtag = hashtag;
    this.state = state;
    this.message = " únete al reto. Hay que publicar el gasto con esta herramienta http://bit.ly/2gjaQPx"
  }

  var estados = {
    "Aguascalientes": "@CarlosLozanoAgs",
    "Baja California": "@KIKOVEGA_",
    "Baja California Sur": "@cmendozadavis",
    "Campeche": "@alitomorenoc",
    "Chiapas": "@VelascoM_",
    "Chihuahua": "@Javier_Corral",
    "Coahuila": "@rubenmoreiravdz",
    "Colima": "@nachoperaltacol",
    "Ciudad de México": "@ManceraMiguelMX",
    "Durango": "@AispuroDurango",
    "México": "@eruviel_avila",
    "Hidalgo": "@omarfayad",
    "Guanajuato": "@miguelmarquezm",
    "Guerrero": "@HectorAstudillo",
    "Jalisco": "@AristotelesSD",
    "Michoacán": "@Silvano_A",
    "Morelos": "@gracoramirez",
    "Nayarit": "@RobertoSandoval",
    "Nuevo León": "@JaimeRdzNL",
    "Oaxaca": "@GabinoCue",
    "Puebla": "@RafaMorenoValle",
    "Querétaro": "@PanchDominguez",
    "Quintana Roo": "@CarlosJoaquin",
    "San Luis Potosí": "@JMCarrerasGob",
    "Sinaloa": "@malovamx",
    "Sonora": "@claudiapavlovic",
    "Tabasco": "@nunezarturo",
    "Tamaulipas": "@fgcabezadevaca",
    "Tlaxcala": "@GobTlaxcala",
    "Veracruz": "@GobiernoVer",
    "Yucatán": "@RolandoZapataB",
    "Zacatecas": "@ATelloC"
  }




  CreateTweet.prototype.run = function(){
    var title =""
    if( this.state == "Ciudad de México" ){
      title = "Jefe de Gobierno"
    }else{
      title = "Gobernador"
    }

    this.text = [title, estados[this.state] + this.message].join(" ")


    //this.text = tweets[this.state]
    this.el = $("<a class='twitter-share-button' data-size='large' data-count='none'> <p> Mejora el #PublicidadAbierta </p> </a>")
    this.el.attr( "href", "https://twitter.com/intent/tweet?hashtags=PublicidadAbierta&text=" + encodeURI(this.text) )

    var a = "https://twitter.com/intent/tweet?hashtags=PublicidadAbierta&text=" + encodeURI(this.text) 
    //this.el.attr( "hashtags", "https://twitter.com/intent/tweet?text=" + encodeURI(this.text) )
    return { 'el': this.el, 'href': a, 'text': this.text, 'tweet': this.text } ;
  }


  var print_tweet = function(city){
    var create_tweet = new CreateTweet(city, '#PublicidadAbierta')
    var tweet = create_tweet.run()
    $(".tw-texto").text( tweet.text )
    //$(".tw-texto").text( tweet.tweet )

    //$('.twitter-share-button').remove()
    //$(".av_textblock_section #llamado").append(tweet.el)
    $(".av_textblock_section#tweet_area .avia-button-2#enviar_tweet").attr("href", tweet.href )
  }

  var successFunction = function(position) {
    var lat = position.coords.latitude
      , lng = position.coords.longitude;
    
    search_city(lat, lng, function(data, error){
      if(data && data.long_name in estados ){
        print_tweet(data.long_name)
      }else{
        errorFunction()
      }
    })
  }

  var errorFunction = function(){
      console.log("Geocoder failed");

      var select = "<select id='state_choise'>"
        select += "<option value=''> Selecciona un estado </states>"
      for( var key in estados){
        select += "<option value='" + key + "'>" + key + "</states>"
      }

      select += "</select>"

      $(".av_textblock_section#tweet_area").append(select)
  }

  var search_city = function(lat, lng, cb) {
    var geocoder = new google.maps.Geocoder()
      , latlng = new google.maps.LatLng(lat, lng);

    var get_city = function(result){
      for (var i=0; i<result.address_components.length; i++) {
        for (var b=0;b<result.address_components[i].types.length;b++) {
        //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
          if (result.address_components[i].types[b] == "administrative_area_level_1") {
              return result.address_components[i]; //this is the object you are looking for
          }
        }
      }
    }

    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          var city = get_city(results[0])
          cb( { short_name: city.short_name, long_name: city.long_name }, null)

        } else {
          cb( null, { code: 404, message: "No results found" })
        }
      } else {
        cb( null, { code: status, message: "Geocoder failed due to: " + status })
      }
    });
  }