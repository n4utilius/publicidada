function sep100(somenum,usa){
  var dec = String(somenum).split(/[.,]/)
     ,sep = usa ? ',' : '.'
     ,decsep = usa ? '.' : ',';

  return xsep(dec[0],sep) + (dec[1] ? decsep+dec[1] :'');

  function xsep(num,sep) {
    var n = String(num).split('')
       ,i = -3;
    while (n.length + i > 0) {
        n.splice(i, 0, sep);
        i -= 3;  //<== here
    }
    return n.join('');
  }
}

var init = function(data_url, p, content){
  d3.selectAll("svg").remove();
  d3.select(".tooltip").remove();
  // Mike Bostock "margin conventions"

  var w = $( window ).width(), message;

  if(w < 760){
    margin = {top: 20, right: 20, bottom: 50, left: 10};

    width = w - margin.left - margin.right,
    height = (w * 0.66) - margin.top - margin.bottom;
  }else{
    margin = {top: 20, right: 20, bottom: 50, left: 80};
    width = 760 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;
  }

  // D3 scales = just math
  // x is a function that transforms from "domain" (data) into "range" (usual pixels)
  // domain gets set after the data loads
  x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .1);

  y = d3.scale.linear()
      .range([height, 0]);

  // D3 Axis - renders a d3 scale in SVG
  xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var formatPercent = d3.format("0f");
  yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(10)
      .tickFormat(function(d) { return sep100((parseInt(d, 10) / 1000000), true) });


  div = d3.select(content).append("div") 
        .attr("class", "tooltip")

  // create an SVG element (appended to body)
  svg = d3.select(content).append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .append("text") // just for the title (ticks are automatic)
  if(w >= 760){
    svg.append("g")
        .attr("class", "y axis")
      .append("text") // just for the title (ticks are automatic)
        .attr("transform", "rotate(-90)") // rotate the text!
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Millones de Pesos");

  }

  svg.append("text")      // text label for the x axis
          .attr("x", width / 2 )
          .attr("y", height + 40 )
          .style("text-anchor", "middle")
          .text("Deciles de cancelaciones de impuestos.");

  // d3.tsv is a wrapper around XMLHTTPRequest, returns array of arrays (?) for a TSV file
  // type function transforms strings to numbers, dates, etc.


  d3.tsv(data_url, type, function(error, data) {
    max = data[data.length -1]
    min = data[0]

    if( $( window ).width() < 760){
      d3.select("#message")
        .html("<p>" + p + "% de los sujetos a los que se les cancela impuestos acumulan un total de $" + min["monto"].toLocaleString().replace(/\./g, ',') + " <b> ¿Se te hace mucho? </b> </p>")

    }else{
      div.append("text")
         .html( "<h3>" + p + "% de los sujetos a los que se les cancela impuestos acumulan un total de  $" + min["monto"].toLocaleString().replace(/\./g, ',') + " </h3> " + 
                "<h2> ¿Se te hace mucho? </h2>" + "<h2>▼</h2>" )
    }
    replay(data, max);
  });
}


function type(d) {
  // + coerces to a Number from a String (or anything)
  d.monto = +d.monto;
  return d;
}

function replay(data, max) {
  var slices = [];
  for (var i = 0; i < data.length; i++) {
    slices.push(data.slice(0, i+1));
  }

  var slice = slices.shift()
  draw(slice, max);

  setTimeout(function(){
    if($( window ).width() < 760){
      d3.select("#message")
        .html( "<p> Pues hay otro 10% que retiene más del <b>" + max.porcentaje + "%</b> de los recursos de esta figura fiscal, que equivale a  " + 
               "<b>$" + max["monto"].toLocaleString().replace(/\./g, ',') + "</b></p>")
    }else{
      div.html("<h3> Pues hay otro 10% que retiene más del " + max.porcentaje + "% de los recursos de esta figura fiscal, que equivale a </h3> " + 
               "<h1> $" + max["monto"].toLocaleString().replace(/\./g, ',') + "► </h1>")
    }

    slices.forEach(function(slice, index){
      setTimeout(function(){
        draw(slice, max);
      }, index * 400);
    });
  }, 3000)

}

var randomColor = (function(){
  var golden_ratio_conjugate = 0.618033988749895;
  var h = Math.random();

  var hslToRgb = function (h, s, l){
      var r, g, b;

      if(s == 0){
          r = g = b = l; // achromatic
      }else{
          function hue2rgb(p, q, t){
              if(t < 0) t += 1;
              if(t > 1) t -= 1;
              if(t < 1/6) return p + (q - p) * 6 * t;
              if(t < 1/2) return q;
              if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
              return p;
          }

          var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
          var p = 2 * l - q;
          r = hue2rgb(p, q, h + 1/3);
          g = hue2rgb(p, q, h);
          b = hue2rgb(p, q, h - 1/3);
      }

      return '#'+Math.round(r * 255).toString(16)+Math.round(g * 255).toString(16)+Math.round(b * 255).toString(16);
  };
  
  return function(){
    h += golden_ratio_conjugate;
    h %= 1;
    return hslToRgb(h, 0.5, 0.60);
  };
})();

function draw(data, max) {
  // measure the domain (for x, unique decils) (for y [0,maxFrequency])
  // now the scales are finished and usable
  x.domain(data.map(function(d) { return d.decil; }));
  y.domain([0, d3.max(data, function(d) { return d.monto; })]);

  // another g element, this time to move the origin to the bottom of the svg element
  // someSelection.call(thing) is roughly equivalent to thing(someSelection[i])
  //   for everything in the selection\
  // the end result is g populated with text and lines!
  svg.select('.x.axis').transition().duration(1000).call(xAxis);

  // same for yAxis but with more transform and a title
  svg.select(".y.axis").transition().duration(1000).call(yAxis)

  // THIS IS THE ACTUAL WORK!
  var bars = svg.selectAll(".bar").data(data, function(d) { return d.decil; }) // (data) is an array/iterable thing, second argument is an ID generator function

  bars.exit()
    .transition()
      .duration(300)
    .attr("y", y(0))
    .attr("height", height - y(0))
    .style('fill-opacity', 1e-6)
    .remove();

  colors = d3.scale.category20()
  // data that needs DOM = enter() (a set/selection, not an event!)
  bars.enter().append("rect")
    .attr("class", "bar")
    .attr("y", y(0))
    .style({fill: randomColor})
    .attr("height", height - y(0))
    .on("mouseover", function(d) {    
      var mdp = Math.trunc( parseInt(d.monto, 10)  / 1000000) 
                       
      if(mdp < 1) monto = "$" + sep100( parseInt(d.monto, 10), true ) 
      else monto = sep100(mdp, true) + " Millones de Pesos"

      if($( window ).width() < 760){
        d3.select("#message")
            .html( "<p>" + monto + " <br> <b>" + d.porcentaje + "%</b> </p>" )
      }else{
        div.transition()    
            .duration(200)    
            .style("opacity", .6);    
        div .html( "<h1>" + monto + " <br> <b>" + d.porcentaje + "%</b> </h1>")  
      }

    })    
    .on("mouseout", function(d){

        if($( window ).width() < 760){
          d3.select("#message")
            .html( "<p> Pues hay otro 10% que retiene más del <b>" + max.porcentaje + "%</b> de los recursos de esta figura fiscal, que equivale a  " + 
                   "<b>$" + max["monto"].toLocaleString().replace(/\./g, ',') + "</b></p>")
        }else{
          div.transition()
            .duration(200)    
            .style("opacity", .6);
          div.html("<h3> Pues hay otro 10% que retiene más del " + max.porcentaje + "% de los recursos de esta figura fiscal, que equivale a </h3> " + 
                   "<h1> $" + max["monto"].toLocaleString().replace(/\./g, ',') + "► </h1>")
        }
    })
      
  // the "UPDATE" set:
  bars.transition().duration(300).attr("x", function(d) { return x(d.decil); }) // (d) is one item from the data array, x is the scale object from above
    .attr("width", x.rangeBand()) // constant, so no callback function(d) here
    .attr("y", function(d) { return y(d.monto); })
    .attr("height", function(d) { return height - y(d.monto); }); // flip the height, because y's domain is bottom up, but SVG renders top down

}